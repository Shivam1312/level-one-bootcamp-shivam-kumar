//Write a program to find the volume of a tromboloid using one function
#include <stdio.h>
int main()
{
  float h,b,d,vol;
  printf("Enter values of h,b and d\n");
  scanf("%f%f%f",&h,&b,&d);
  vol=((1.0/3.0)*((h*d)+d))/b;
  printf("Volume of tromboloid is %f.\n",vol);
  return 0;
}  