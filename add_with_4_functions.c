//Write a program to add two user input numbers using 4 functions.
#include <stdio.h>
int input()
{
  int a;
  printf("Enter Number:\n");
  scanf("%d",&a);
  return(a);
}
int compute(int n1,int n2)
{
  int sum=0;
  sum=n1+n2;
  return(sum);
}
void display(int x,int y,int s)
{
  printf("Sum of %d and %d is %d.\n",x,y,s);
}
int main()
{
int a1,b1,s1;
  a1=input();
  b1=input();
  s1=compute(a1,b1);
  display(a1,b1,s1);
  return 0;
}  
