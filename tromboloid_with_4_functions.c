//WAP to find the volume of a tromboloid using 4 functions.
#include <stdio.h>
float input()
{
 float a;
 scanf("%f",&a);
 return(a);
 }
float compute(float h1,float b1,float d1)
{
 float vol;
 vol=((1.0/3.0) * ((h1 * d1)+d1))/b1;
 return(vol);
}
void display(float volume)
{
 printf("Volume of the tromboloid is %f.\n",volume);
}
int main()
{
 float h,b,d,v;
 printf("Enter value of h:\n");
 h=input();
 printf("Enter value of b:\n");
 b=input();
 printf("Enter value of d:\n");
 d=input();
 v=compute(h,b,d);
 display(v);
 return 0;
} 