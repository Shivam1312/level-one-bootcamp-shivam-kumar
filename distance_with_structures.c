//WAP to find the distance between two points using structures and 4 functions.
#include <stdio.h>
#include <math.h>
struct p1
{
  int x1;
  int y1;
};
struct p1 point1;
struct p2
{
  int x2;
  int y2;
};
struct p2 point2;
int input()
{
  int a;
  scanf("%d",&a);
  return(a);
}
float compute()
{
  float d;
  d=sqrt(pow(point2.x2-point1.x1,2)+pow(point2.y2-point1.y1,2));
  return(d);
}
void display(float d)
{
  printf("Distance between the given points is %f.",d);
}
int main()
{
  float dist;
  printf("Enter the x-coordinate of 1st point:\n");
  point1.x1=input();
  printf("Enter the y-coordinate of 1st point:\n");
  point1.y1=input();
  printf("Enter the x-coordinate of 2nd point:\n");
  point2.x2=input();
  printf("Enter the y-coordinate of 2nd point:\n");
  point2.y2=input();
  dist= compute();
  display(dist);
  return 0;
}
