//WAP to find the distance between two point using 4 functions.
#include <stdio.h>
#include <math.h>
int input()
{
  int a;
  scanf("%d",&a);
  return(a);
 }
float compute(int x1,int y1, int x2, int y2)
{
  float dis;
  dis=sqrt(pow(x2-x1,2)+pow(y2-y1,2));
  return(dis);
}
void display(float d)
{
  printf("Distance between the given points is %f.\n",d);
}
int main()
{
  int x1,y1,x2,y2;
  float dist;
  printf("Enter the x-coordinate of 1st point:\n");
  x1=input();
  printf("Enter the y-coordinate of 1st point:\n");
  y1=input();
  printf("Enter the x-coordinate of 2nd point:\n");
  x2=input();
  printf("Enter the y-coordinate of 2nd point:\n");
  y2=input();
  dist=compute(x1,y1,x2,y2);
  display(dist);
  return 0;
}
