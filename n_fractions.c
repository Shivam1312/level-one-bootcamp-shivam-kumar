//WAP to find the sum of n fractions.
#include <stdio.h>
struct fraction
{
  int num;
  int den;
};
int input ( )
{
 int n;
 printf("Enter value of n\n");
 scanf("%d",&n);
 return(n);
}
struct fraction inputone()
{
  struct fraction f;
  scanf("%d/%d",&f.num,&f.den);
  return(f);
}
int cal_lcm( int fd,int sumd)
{
  int i,q,s;
  if(fd>sumd)
  {
    for(i=1;i<=fd;i++)
    {
      s=fd*i;
      if(s%sumd==0)
      {
        q=s;
        break;
       }
     }
  }
  else if(fd<sumd)
  {
   for(i=1;i<=sumd;i++)
   {
     s=sumd*i;
     if(s%fd==0)
     { 
       q=s;
       break;
      }
     }
   }
  else
  {
    q=fd;
  }
  return(q);
} 
void display(struct fraction f[ ], int n, struct fraction sum)
{
  printf("Sum of fractions is \n");
  for(int i=0;i<n;i++)
  {
    if(i==(n-1))
    printf("%d/%d ",f[i].num,f[i].den);
    else
    printf("%d/%d + ",f[i].num,f[i].den);
  }
  printf("= %d/%d",sum.num,sum.den);
}
struct fraction computen (struct fraction f[ ], int n)
{
  struct fraction sum;
  int lcm;
  sum.num=0;
  sum.den=1;
  for(int i=0;i<n;i++)
  {
    lcm=cal_lcm(f[i].den,sum.den);
    int f1_num,f2_num,f_num;
    f1_num=f[i].num*(lcm/f[i].den);
    f2_num=sum.num*(lcm/sum.den);
    sum.num=f1_num+f2_num;
    sum.den=lcm;
   }
   int l=1;
   for(int i=2;i<=sum.num && i<=sum.den;i++)
   {
    if(sum.num%i==0 && sum.den%i==0)
      l=i;
    }
  sum.num=sum.num/l;
  sum.den=sum.den/l;
  return(sum);
}
struct fraction inputn(struct fraction f[ ], int n)
{
   for(int i=0;i<n;i++)
  {
   printf("Enter a fraction\n");
   f[i]=inputone();
  }  
}
int main()
{
  int n;
  n=input();
  struct fraction f[n];
  struct fraction sum;
  inputn(f,n);
  sum=computen(f,n);
  display(f,n,sum);
  return 0;
}
