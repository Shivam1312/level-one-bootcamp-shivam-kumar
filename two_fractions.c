//WAP to find the sum of two fractions.
#include <stdio.h>
struct fraction
{
  int num;
  int den;
};
struct fraction input()
{
  struct fraction f;
  scanf("%d/%d",&f.num,&f.den);
  return(f);
}
int cal_lcm(int f1d,int f2d)
{
  int i,q,s;
  if(f1d>f2d)
  {
    for(i=1;i<=f1d;i++)
    {
      s=f1d*i;
      if(s%f2d==0)
      {
        q=s;
        break;
       }
     }
  }
  else if(f1d<f2d)
  {
   for(i=1;i<=f2d;i++)
   {
     s=f2d*i;
     if(s%f1d==0)
     { 
       q=s;
       break;
      }
     }
   }
  else
  {
    q=f1d;
  }
  return(q);
}
struct fraction gcd (struct fraction f1,struct fraction f2)
{
 struct fraction f;
 int f1_num,f2_num,f_num,l=1,lcm;
 lcm=cal_lcm(f1.den,f2.den);
 f1_num=f1.num*(lcm/f1.den);
 f2_num=f2.num*(lcm/f2.den);
 f_num=f1_num+f2_num;
 for(int i=2;i<=f_num && i<=lcm;i++)
 {
   if(f_num%i==0 && lcm%i==0)
   l=i;
 }
 f.num=f_num/l;
 f.den=lcm/l;
 return(f);
} 
void display(struct fraction f1,struct fraction f2,struct fraction f3)
{
printf("The sum of %d/%d and %d/%d is %d/%d.\n",f1.num,f1.den,f2.num,f2.den,f3.num,f3.den);
 }
int main(void)
{
  struct fraction f1,f2,f3;
  int w;
  printf("Enter 1st fraction\n");
  f1=input();
  printf("Enter 2nd fraction\n");
  f2=input();
  f3=gcd(f1,f2);
  display(f1,f2,f3);
  return 0;
}

